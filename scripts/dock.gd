extends Node2D

var id = 0
var selected = false

onready var gameNode = get_parent().get_parent()

func _ready():
	pass

func setItem(pid):
	id = pid
	selected = false
	get_node("item").set_frame(id)
	if id == 1:
		get_node("costs").set_text("50")
	else:
		get_node("costs").set_text("100")

func _on_yes_area_enter( area ):
	if area.get_name() == "pirateArea" and selected == false:
		selected = true
		if id == 0:
			if gameNode.subCash(100):
				gameNode.spawnPirate()
		else:
			if gameNode.subCash(50):
				area.get_parent().upgrade()
		gameNode.finishDock()

func _on_no_area_enter( area ):
	if area.get_name() == "pirateArea" and selected == false:
		selected = true
		gameNode.finishDock()
