# OMG Buccaneer by mago 2017
#
# License:
# Do what you want cause a pirate is free - you are a pirate :)
#
# Just kidding:
# Music by Ozzed (ozzed.net) https://creativecommons.org/licenses/by-sa/3.0/
# All other shit release under https://creativecommons.org/licenses/by-nc/3.0/
#
# Note:
# This jam I´ve tried to avoid global autoloaded scripts. Ended up in an unstructured mess.
# But anyhow, I hope you enjoy the source code and the game.


extends Control

const BASE_LIVE = 50
const BASE_ATTACKSPEED = 1.5
const BASE_DAMAGE = 10
const BASE_ACC = 0.5
const FACTOR_LIVE = 5
const FACTOR_ATTACKSPEED = 0.02
const FACTOR_DAMAGE = 2.5
const FACTOR_ACC = 0.05

var interfaces = 0
var selectedPirate = 0
var tempPlayerHealth = 0
var level = 0
var noOfPirates = 0
var money = 100
var tempMoney = 0
var roughSea = false

onready var shipNode = get_node("static/ship")
onready var hpNode = get_node("static/hud/hp")
onready var moneyNode = get_node("static/hud/money")
onready var enemyNode = get_node("static/enemyHolder/enemy")
var pirateNodeArray = [null, null, null, null]


func _fixed_process(delta):
	#Health Change
	if tempPlayerHealth != shipNode.life:
		tempPlayerHealth = shipNode.life
		#Update HUD
		hpNode.set_text(str(int(tempPlayerHealth)))
		
		#GameOver Control
		if tempPlayerHealth <= 0:
			gameOver()
	#Money Change
	if tempMoney != money:
		tempMoney = money
		moneyNode.set_text(str(money))

func eventRoughSea():
	if not get_node("roughSeaTimer").is_processing():
		get_node("roughSeaTimer").start()
		roughSea = true
		shipNode.setRoughSea(true)
		get_node("parallax/sea/rocks").show()

func _on_roughSeaTimer_timeout():
	roughSea = false
	shipNode.setRoughSea(false)
	get_node("parallax/sea/rocks").hide()

func subCash(amnt):
	if amnt > money:
		return false
	else:
		money -= amnt
		return true

func spawnPirate():
	shipNode.spawnPirate()

func finishDock():
	print("bye")
	get_node("animationDock").play("out")
	get_node("spawnTimer").start()

func hitEnemy(dmg):
	enemyNode.shipHit(dmg)

func gameOver():
	get_node("animationPlayer").play("gameOver")
	get_node("static/end/level").set_text("Level "+str(level))
	enemyNode.setActive(false)
	playerCanAttack(false)

func _ready():
	print("game")
	set_fixed_process(true)
	nextLevel()

func playerCanAttack(val):
	shipNode.playerCanAttack = val

func attackPlayer(dmg):
	shipNode.takeDamage(dmg)

func nextLevel():
	level += 1
	get_node("static/hud/level").set_text("Level "+str(level))

	#Tutorial
	if level == 1:
		get_node("static/hud/levelNotify").set_text("Capitain  man the cannon")
		enemyNode.setShipAttrib(30, 1.5, 10, 0.3)
		get_node("animationEnemy").play("enemyshipenter")
		get_node("static/enemyHolder/enemy").setActive(true)
	elif level == 2:
		get_node("static/hud/levelNotify").set_text("A shop    recruite a new pirate")
		get_node("animationDock").play("in")
		get_node("static/dock").setItem(0)
	elif level == 3:
		get_node("static/hud/levelNotify").set_text("Go downstairs to repair the ship")
		enemyNode.setShipAttrib(30, 1.5, 15, 0.4)
		get_node("animationEnemy").play("enemyshipenter")
		get_node("static/enemyHolder/enemy").setActive(true)
	elif level == 4:
		get_node("static/hud/levelNotify").set_text("Go to the crates to heal")
		enemyNode.setShipAttrib(40, 1.6, 20, 0.5)
		get_node("animationEnemy").play("enemyshipenter")
		get_node("static/enemyHolder/enemy").setActive(true)
	elif level == 5:
		get_node("static/hud/levelNotify").set_text("Every 5th level is a shop")
		get_node("animationDock").play("in")
		get_node("static/dock").setItem(1)
	elif level == 6:
		get_node("static/hud/levelNotify").set_text("Rough See go to the wheel")
		enemyNode.setShipAttrib(50, 1.5, 10, 0.6)
		eventRoughSea()
		get_node("animationEnemy").play("enemyshipenter")
		get_node("static/enemyHolder/enemy").setActive(true)
	elif level == 10:
			get_node("static/hud/levelNotify").set_text("")
			get_node("animationDock").play("in")
			get_node("static/dock").setItem(0)
	elif level == 30:
			get_node("static/hud/levelNotify").set_text("")
			get_node("animationDock").play("in")
			get_node("static/dock").setItem(0)
	elif level % 5 == 0:
			get_node("static/hud/levelNotify").set_text("")
			get_node("animationDock").play("in")
			get_node("static/dock").setItem(1)
	#Main game
	else: 
		if level % 7 == 0 and level != 7:
			eventRoughSea()
		#setShipAttrib(plive, pattackSpeed, pattackDamage, pattackAcc)

		get_node("static/hud/levelNotify").set_text("")
		enemyNode.setShipAttrib(BASE_LIVE + FACTOR_LIVE*(level - 6), BASE_ATTACKSPEED - FACTOR_ATTACKSPEED*(level - 6), BASE_DAMAGE + FACTOR_DAMAGE*(level - 6), BASE_ACC + FACTOR_ACC*(level - 6))
		get_node("animationEnemy").play("enemyshipenter")
		get_node("static/enemyHolder/enemy").setActive(true)

func enemyDied():
	if not get_node("animationEnemy").is_playing():
		get_node("animationEnemy").play("enemyshipexit")
		get_node("static/enemyHolder/enemy").setActive(false)
		money += 25
		shipNode.playerCanAttack = false
		get_node("spawnTimer").start()

func getPirate():
	return selectedPirate

func getPirateObj():
	return pirateNodeArray[selectedPirate]

func selectPirate(id):
	selectedPirate = id

func registerPirate(obj):
	print("pirate registered")
	var scoreBoard = load("res://scene/pirateInfo.tscn").instance()
	get_node("static/hud/valign").add_child(scoreBoard)
	scoreBoard.setId(noOfPirates)
	pirateNodeArray[noOfPirates] = obj
	noOfPirates += 1
	return scoreBoard

func unregisterPirate(id):
	shipNode.pirateNodeArray[id] = null
	
func registerInterface(obj):
	var tmp = interfaces
	interfaces += 1
	print("interface registered: "+ str(tmp))
	return tmp

func _on_spawnTimer_timeout():
	nextLevel()

