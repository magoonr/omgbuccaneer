extends Area2D

var damage = 0

func _ready():
	get_node("sound").play("hit")

func _on_timeOut_timeout():
	queue_free()

func setDamage(dmg):
	damage = dmg

func _on_explosion_area_enter( area ):
	if area.get_name() == "pirateArea":
		area.get_parent().takeDamage(damage)
