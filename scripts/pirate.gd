extends Sprite
const UPGRADE_FACTOR = 1.5
const DOWNGRADE_FACTOR = 0.9

var life = 100
var sailing = 0
var cannon = 0
var repair = 0
onready var gameNode = get_parent().get_parent().get_parent().get_parent()
var panelNode = null
var ownId = null

var animationPlaying = "idle"

func _ready():
	randomize()
	panelNode = gameNode.registerPirate(self)
	get_node("animation").play(animationPlaying)
	
	if panelNode != null:
		sailing = int(rand_range(1,10))*10
		cannon = int(rand_range(1,10))*10
		repair = int(rand_range(1,10))*10
		panelNode.setHp(life)
		panelNode.setSailing(sailing)
		panelNode.setCannon(cannon)
		panelNode.setRepair(repair)
		ownId = panelNode.getId()
		if ownId > 0:
			get_node("sound").play("spawn")

func animation(isWalking):
	if isWalking:
		if animationPlaying != "walk":
			animationPlaying = "walk"
			get_node("animation").play(animationPlaying)
	else:
		if animationPlaying != "idle":
			animationPlaying = "idle"
			get_node("animation").play(animationPlaying)


func upgradeSailing():
	sailing += 5
	if sailing > 999:
		sailing = 999
	panelNode.setSailing(sailing)

func upgradeCannon():
	cannon += 2
	if cannon > 999:
		cannon = 999
	panelNode.setCannon(cannon)

func upgradeRepair():
	repair += 5
	if repair > 999:
		repair = 999
	panelNode.setRepair(repair)

func upgrade():
	get_node("sound").play("upgrade")
	sailing = int(sailing * UPGRADE_FACTOR)
	if repair > 999:
		repair = 999
	cannon = int(cannon * UPGRADE_FACTOR)
	if cannon > 999:
		cannon = 999
	repair = int(repair * UPGRADE_FACTOR)
	if repair > 999:
		repair = 999
	life = 100
	panelNode.setHp(life)
	panelNode.setSailing(sailing)
	panelNode.setCannon(cannon)
	panelNode.setRepair(repair)

func downgrade():
	sailing = int(sailing * DOWNGRADE_FACTOR)
	cannon = int(cannon * DOWNGRADE_FACTOR)
	repair = int(repair * DOWNGRADE_FACTOR)
	panelNode.setSailing(sailing)
	panelNode.setCannon(cannon)
	panelNode.setRepair(repair)

func action(kind):
	get_node("Label").set_text(kind)

func takeHeal():
	life += 10
	if life > 100:
		life = 100
	panelNode.setHp(life)

func takeDamage(dmg):
	life -= dmg
	get_node("sound").play("pirateHit")
	get_node("blood").set_emitting(true)
	downgrade()
	if life <= 0:
		gameNode.unregisterPirate(ownId)
		panelNode.queue_free()
		remove_child(get_node("pirateArea"))
		hide()
	else:
		panelNode.setHp(life)
