extends Control

var pirateId = null
onready var gameNode = get_parent().get_parent().get_parent().get_parent()
onready var activeImageNode = get_node("activeImage")

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	if gameNode.getPirate() == pirateId:
		activeImageNode.show()
	else:
		activeImageNode.hide()

func setId(id):
	pirateId = id
	get_node("portrait").set_frame(pirateId)

func _on_TextureButton_button_down():
	if pirateId != null:
		gameNode.selectPirate(pirateId)
		print(pirateId)

func getId():
	return pirateId

func setHp(val):
	get_node("hp").set_text(str(int(val)))

func setSailing(val):
	get_node("sailingSkill").set_text(str(int(val)))

func setCannon(val):
	get_node("cannonSkill").set_text(str(int(val)))

func setRepair(val):
	get_node("repairSkill").set_text(str(int(val)))