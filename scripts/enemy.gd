extends Node2D


var shipIsReady = false #Ship in position to attack
var shipIsActive = false #Ship is in scene or entering it
var shipIsAlive = false #Ship is alive not dead

var live = 10 #Hitpoints
var tempLive = 0 #Hitpoints last frame
var attackSpeed = 1 #Attack speed in seconds
var attackDamage = 1 #Damage per Hit
var attackAcc = 1.0 #Attack Accuracy 0.0 - 1.0

onready var attackTimer = get_node("attackTimer")
onready var gameNode = get_parent().get_parent().get_parent()
onready var bombStartPos = get_node("bombStart").get_pos()
onready var bombEndPos = get_node("bombEnd").get_pos()

func _ready():
	randomize()
	set_fixed_process(true)
	
func _fixed_process(delta):
	if shipIsActive:
		if shipIsReady:
			#Attack if possibru
			if not attackTimer.is_processing():
				attackTimer.start()
			#Update HP
			if tempLive != live:
				tempLive = live
				get_node("hp").set_text(str(live))


func shipHit(dmg):
	live -= dmg
	if live < 0:
		live = 0
		gameNode.enemyDied()
	var hitPos = Vector2(rand_range(bombStartPos.x, bombEndPos.x), rand_range(bombStartPos.y, bombEndPos.y))
	var explosion = load("res://scene/explosion.tscn").instance()
	add_child(explosion)
	explosion.set_pos(hitPos)

func setShipAttrib(plive, pattackSpeed, pattackDamage, pattackAcc):
	live = plive
	attackDamage = pattackDamage
	attackAcc = pattackAcc
	attackTimer.set_wait_time(pattackSpeed)
	shipIsReady = false 
	shipIsActive = false 
	shipIsAlive = false 

func shipReady():
	print("ready")
	shipIsReady = true
	gameNode.playerCanAttack(true)

func setActive(val):
	shipIsActive = val

func _on_attackTimer_timeout():
	var rand = rand_range(0.0, 1.0)
	get_node("muzzle1/animation").play("fire")
	get_node("smallDelay").start()
	if rand < attackAcc:
		gameNode.attackPlayer(attackDamage)

func _on_smallDelay_timeout():
	get_node("muzzle/animation").play("fire")
