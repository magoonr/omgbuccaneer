extends Navigation2D


const REPAIR_BASE = 5
const REPAIR_FACTOR = 0.04
const CANNON_BASE = 10
const CANNON_FACTOR = 0.033
const SEA_DAMAGE = 10
const SEA_DAMAGE_FACTOR = 0.01

# Member variables
const SPEED = 50.0
const MAX_LIFE = 100
var playerCanAttack = false

var life = 100
var begin = [Vector2(), Vector2(), Vector2(), Vector2()]
var end  = [Vector2(), Vector2(), Vector2(), Vector2()]

var cannon1Manned = false
var cannon2Manned = false
var repairManned = false
var steerManned = false
var bodyInHeal = null
var bodyInCannon1 = null
var bodyInCannon2 = null
var bodyInRepair = null
var bodyInSteer = null
var roughSea = false

var pathArray = [[], [], [], []]

onready var bombStartPos = get_node("bombStart").get_pos()
onready var bombEndPos = get_node("bombEnd").get_pos()

onready var pirateNodeArray = [null, null, null, null]

onready var gameNode = get_parent().get_parent()
var pirateArrayIdx = 0

func setRoughSea(val):
	roughSea = val

func takeDamage(dmg):
	life -= dmg
	if life < 0:
		life = 0
	var hitPos = Vector2(rand_range(bombStartPos.x, bombEndPos.x), rand_range(bombStartPos.y, bombEndPos.y))
	var explosion = load("res://scene/explosion.tscn").instance()
	add_child(explosion)
	explosion.setDamage(dmg)
	explosion.set_pos(hitPos)

func takeSeaDamage(dmg):
	if dmg > 0:
		life -= dmg
		if life < 0:
			life = 0

func spawnPirate():
	var pirate = load("res://scene/pirate.tscn").instance()
	get_node("pirates").add_child(pirate)
	pirate.set_pos(Vector2(-5,25))

func _fixed_process(delta):
	if playerCanAttack:
		if cannon1Manned:
			if not get_node("cannon1/cannon1Timer").is_processing():
				get_node("cannon1/cannon1Timer").start()
		if cannon2Manned:
			if not get_node("cannon2/cannon2Timer").is_processing():
				get_node("cannon2/cannon2Timer").start()
	if repairManned:
		if not get_node("underDeck/underDeckTimer").is_processing():
			get_node("underDeck/underDeckTimer").start()
	if roughSea:
		if not get_node("roughSeaDamageTimer").is_processing():
			get_node("roughSeaDamageTimer").start()


func _process(delta):
	if pirateNodeArray[pirateArrayIdx]:
		var path = pathArray[pirateArrayIdx]
		if (path.size() > 1):
			var to_walk = delta*SPEED
			while(to_walk > 0 and path.size() >= 2):
				var pfrom = path[path.size() - 1]
				var pto = path[path.size() - 2]
				var d = pfrom.distance_to(pto)
				if (d <= to_walk):
					path.remove(path.size() - 1)
					to_walk -= d
				else:
					path[path.size() - 1] = pfrom.linear_interpolate(pto, to_walk/d)
					to_walk = 0
			
			var atpos = path[path.size() - 1]
			pirateNodeArray[pirateArrayIdx].set_pos(atpos)
			pirateNodeArray[pirateArrayIdx].animation(true)
			
			if (path.size() < 2):
				path = []
				pirateNodeArray[pirateArrayIdx].animation(false)
		pathArray[pirateArrayIdx] = path

func _update_path():
	var path = pathArray[pirateArrayIdx]
	var p = get_simple_path(begin[pirateArrayIdx], end[pirateArrayIdx], true)
	path = Array(p)
	path.invert()
	pathArray[pirateArrayIdx] = path

func _input(event):
	if (event.type == InputEvent.MOUSE_BUTTON and event.pressed and event.button_index == 1 and event.pos.x < 370):
		pirateArrayIdx = gameNode. getPirate()
		pirateNodeArray[pirateArrayIdx] = gameNode.getPirateObj()
		begin[pirateArrayIdx] = pirateNodeArray[pirateArrayIdx].get_pos()
		end[pirateArrayIdx] = event.pos - get_pos()
		_update_path()

func _ready():
	set_process_input(true)
	set_process(true)
	set_fixed_process(true)


###################################################################################################
# AREA TIMER FUNCTIONS
###################################################################################################

func _on_cannon1Timer_timeout():

	if bodyInCannon1.has_method("upgradeCannon"):
		get_node("cannon1/muzzle/animation").play("fire")
		gameNode.hitEnemy(CANNON_BASE + CANNON_FACTOR * bodyInCannon1.cannon)
		bodyInCannon1.upgradeCannon()

func _on_cannon2Timer_timeout():
	if bodyInCannon2.has_method("upgradeCannon"):
		get_node("cannon2/muzzle/animation").play("fire")
		gameNode.hitEnemy(CANNON_BASE + CANNON_FACTOR * bodyInCannon2.cannon)
		bodyInCannon2.upgradeCannon()

func _on_underDeckTimer_timeout():
	if bodyInRepair.has_method("upgradeRepair"):
		life += (REPAIR_BASE + REPAIR_FACTOR * bodyInRepair.repair)
		if life > MAX_LIFE:
			life = MAX_LIFE
		else:
			bodyInRepair.upgradeRepair()

func _on_healTimer_timeout():
	if bodyInHeal != null:
		if bodyInHeal.has_method("takeHeal"):
			bodyInHeal.takeHeal()
			get_node("heal/healTimer").start()

func _on_roughSeaDamageTimer_timeout():
	if steerManned == false:
		takeSeaDamage(SEA_DAMAGE)
	else:
		if bodyInCannon1 != null:
			bodyInCannon1.upgradeSailing()
			takeSeaDamage(SEA_DAMAGE - SEA_DAMAGE_FACTOR * bodyInSteer.sailing)

###################################################################################################
# AREA HANDLING
###################################################################################################
#Handle Area
func handleArea(area, name):
	if area != null:
		if area.get_parent().has_method("action"):
			area.get_parent().action(name)
		return true
	else:
		return false

#Deck Enter
func _on_underDeck_area_enter( area ):
	if handleArea(area, "repairing"):
		repairManned = true
		bodyInRepair = area.get_parent()
#Deck Exit
func _on_underDeck_area_exit( area ):
	handleArea(area, "")
	repairManned = false
	bodyInRepair = null
	get_node("underDeck/underDeckTimer").stop()

#Cannon1 Enter
func _on_cannon1_area_enter( area ):
	if handleArea(area, "cannon1"):
		cannon1Manned = true
		bodyInCannon1 = area.get_parent()
#Cannon1 Exit
func _on_cannon1_area_exit( area ):
	handleArea(area, "")
	cannon1Manned = false
	bodyInCannon1 = null
	get_node("cannon1/cannon1Timer").stop()

#Cannon2 Enter
func _on_cannon2_area_enter( area ):
	if handleArea(area, "cannon2"):
		cannon2Manned = true
		bodyInCannon2 = area.get_parent()
#Cannon2 Exit
func _on_cannon2_area_exit( area ):
	handleArea(area, "")
	cannon2Manned = false
	bodyInCannon2 = area.get_parent()
	get_node("cannon2/cannon2Timer").stop()

#Steer Enter
func _on_steer_area_enter( area ):
	if handleArea(area, "steer"):
		steerManned = true
		bodyInSteer = area.get_parent()
#Steer Exit
func _on_steer_area_exit( area ):
	handleArea(area, "")
	steerManned = false
	bodyInSteer = null

#Heal Enter
func _on_heal_area_enter( area ):
	if handleArea(area, "heal"):
		get_node("heal/healTimer").start()
		bodyInHeal = area.get_parent()
#Heal Exit
func _on_heal_area_exit( area ):
	handleArea(area, "")
	if bodyInHeal == area.get_parent():
		bodyInHeal = null
	get_node("heal/healTimer").stop()