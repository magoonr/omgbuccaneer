extends Camera2D

func _ready():
	set_fixed_process(true)
	
func _fixed_process(delta):
	var pos = get_pos()
	set_pos(Vector2(pos.x + 1, pos.y))
