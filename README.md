# README #

OMG! Buccaneer is a game writen for the #openJam and the #OMGJam. It tries to unify the 'One Mechanic Game' topic with the 'Leave a mark' theme. Hopefully you enjoy it. Source code is a mess but available as FOSS.

## ABOUT ##

Manage up to 4 legendary pirates and become the best pirate alive, arrr!
Command them with the Mouse. Let them shoot, steer, repair and heal!
The game is all about micro-management, timing and some luck. Attack the enemy but also repair the ship and don´t let the crew die. As if that weren´t enough, some one has to steer the ship. OMG! Buccaneers :)

## CONTROLS ##

Command your crew with the mouse

## LICENSE ##
Do what you want cause a pirate is free - you are a pirate :)
Just kidding:

Music by Ozzed, check out http://ozzed.net/music/ Licensed under creative commons: https://creativecommons.org/LICENSES/BY-SA/3.0/
All other shit release under https://creativecommons.org/licenses/by-nc/3.0/

Tools used: Gimp, the old FOSS version of Aseprite, Audacity, Bfxr  and Godot Engine
